<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.1.1">
    <title>Check-In</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/starter-template/">

    <!-- Bootstrap core CSS -->
<link href="assets/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
<link href="assets/dist/css/starter-template.css" rel="stylesheet">

    <style>
	body{
	/* background-image : url('https://source.unsplash.com/random/1920x1080'); */
	background: rgb(2,0,36);
	background: linear-gradient(90deg, rgba(2,0,36,1) 0%, rgba(9,9,121,1) 13%, rgba(0,212,255,1) 100%);
	min-width:100%;
	max-width:100%;
	background-repeat: no-repeat;
	background-attachment: fixed;
	
	}
	img{
	min-width:100%;
	max-width:100%;
	}
	
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
	  .row{
		  padding:10px;
	   }
	  
    </style>

  </head>
  <body> 
  
 <?php 
 
 class Stuff{
	 
	 public function get_local_ip(){
		 $myIp = getHostByName(getHostName());
		 return $myIp;
	 }
	 
 }
 
 ?>

<main role="main" class="container">
<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-body">
						<h1> Welcome </h1>
							</div>
						</div>
					</div>					
				</div>
  
		<div class="row">
			<div class="col-md-6">
				<div class="card">
					<div class="card-body">
					<?php
					 $localip = new Stuff();					 
					?>
					<p> Your Local IP : </p>
					<p> <?php echo  $localip->get_local_ip(); ?> </p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card">
					<div class="card-body">
					<p><?php 
					echo "Today is " . date("m d, Y") . "</p> <p>";
					echo "Today is " . date("l");
					?></p>
					</div>
				</div>
			
			</div>
		</div>
		<div class="row"> 
				<div class="col-md-12">
					<div class="card">
						<div class="card-body">
								<blockquote class="blockquote mb-0">
								  <p>loading...</p>
								  <footer class="blockquote-footer">
									<cite title="Source Title"></cite>
								  </footer>
								</blockquote>
						</div>	
					</div>
				</div>
		</div>

		<div class="row"> 
				<div class="col-md-12">
					<div class="card">
						<div class="card-body">
					
							<p> 
							<?php
							echo date("l");
							?>
							</p>
							<img id="image_daily" src="https://source.unsplash.com/collection/1168327">
								
						</div>	
					</div>
				</div>
		</div>
</main><!-- /.container -->
<script src="js/quote_api.js"> </script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
      <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery.slim.min.js"><\/script>')</script><script src="../assets/dist/js/bootstrap.bundle.min.js"></script>
</html>
